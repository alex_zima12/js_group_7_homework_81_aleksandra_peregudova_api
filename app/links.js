const router = require("express").Router();
const {nanoid} = require("nanoid");
const Link = require("../models/Liks");

const createRouter = () => {
    router.get("/:route", async (req, res) => {
        const route = req.params.route;
        const result = await Link.findOne({shortUrl: route});
        if (result) {
            res.status(301).redirect(result.originalUrl);
        } else {
            res.sendStatus(404);
        }
    });
    router.post("/", async (req, res) => {
        const shortUrl = nanoid(7);
        const linkData = {...req.body, shortUrl};
        const link = new Link(linkData);
        try {
            await link.save();
            res.send(link.shortUrl);
        } catch (e) {
            res.status(400).send(e);
        }
    });
    return router;
};

module.exports = createRouter;